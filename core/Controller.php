<?php
declare(strict_types=1);

namespace Core;

/*
 * Base Controller
 */
abstract class Controller
{
    /**
     * Default layout
     * @var string
     */
    public $layout = 'layout';

    /**
     * @param string $view
     * @param array $parameters
     */
    public function renderTemplate(string $view, array $parameters): void
    {
        (new View())->render($this->layout, $view, $parameters);;
    }
}