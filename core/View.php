<?php
declare(strict_types=1);

namespace Core;

/**
 * View
 */
class View
{
    /**
     * @param $layout
     * @param $view
     * @param array $parameters
     */
    public static function render(string $layout, string $view, array $parameters = []): void
    {
        extract($parameters);
        $view = APP . "/view/$view.php";
        ob_start();
        if (is_file($view)) {
            require $view;
        } else {
            echo "View <b>$view</b> not found";
        }
        $content = ob_get_clean();
        require APP . "/view/$layout.php";
    }
}
