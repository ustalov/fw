<?php
declare(strict_types=1);

namespace Core;

/*
 * Base Model
 */
abstract class Model extends Validate
{
    protected $pdo;
    protected $table;
    protected $fields = [];
    private $schema;

    /**
     * Model constructor.
     * @param array $schema
     */
    public function __construct(array $schema)
    {
        $db = require ROOT . '/config/db.php';

        // Set passed variables/parametrs to fields array
        foreach ($schema as $name => $type) {
            $this->fields[$name] = ['value' => null, 'type' => $type];
        }

        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        ];

        $this->pdo = new \PDO($db['dsn'], $db['user'], $db['pass'], $options);
        $this->schema = $schema;
    }

    /**
     * Find all records in table
     * @return array
     */
    public function findAll(): array
    {
        $sql = "SELECT * FROM {$this->table}";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * Find one record by column name and value
     * @param $field
     * @param $value
     * @return mixed
     */
    public function findOne(string $field, $value)
    {
        $sql = "SELECT * FROM {$this->table} ";
        $sql .= "WHERE {$field} = :{$field}";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam($field, $value);
        $stmt->execute();
        return $stmt->fetch();
    }

    /**
     * Insert/Update the record
     * @return bool
     */
    public function save(): bool
    {
        foreach ($this->fields as $field => $f) {
            if ($field != 'id' && $f['value'] != null) {
                $fields[] = $field;
                $set[] = "{$field} = :{$field}";
            }
        }
        $sql = "INSERT INTO {$this->table} (" . implode(',', $fields) . ") ";
        $sql .= "VALUES(:" . implode(',:', $fields) . ") ";
        $sql .= "ON DUPLICATE KEY UPDATE " . implode(',', $set);

        $stmt = $this->pdo->prepare($sql);

        foreach ($this->fields as $field => $f) {
            if ($f['value'] != null) {
                $stmt->bindValue(":{$field}", $f['value'], $f['type']);
            }
        }
        return $stmt->execute();
    }

    /**
     * Delete one or mass records by id(s)
     * @param mixed $id
     */
    public function delete($id): void
    {
        if (!is_array($id)) $id = [$id];
        $id = array_map([$this->pdo, 'quote'], $id);
        $id = implode(',', $id);

        $sql = "DELETE FROM {$this->table} WHERE id IN ($id)";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
    }

    /**
     * Set variables child model
     * @param string $name
     * @param mixed $value
     */
    public function __set(string $name, $value): void
    {
        $this->fields[$name]['value'] = $value;
    }
}