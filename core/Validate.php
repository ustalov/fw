<?php
declare(strict_types=1);

namespace Core;

/**
 * Validate
 */
class Validate
{
    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @param string $field
     * @param string $error
     */
    public function addError(string $field, string $error): void
    {
        $this->errors[$field] = $error;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
