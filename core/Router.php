<?php
declare(strict_types=1);

namespace Core;

/*
 * Router
 */
class Router
{
    /**
     * Available routes
     * @var array
     */
    protected $routes = [];

    /**
     * Current route
     * @var array
     */
    protected $route = [];

    /**
     * @param string $route
     * @param array $params
     */
    public function add(string $route, array $params): void
    {
        $$route = rtrim($route, '/');
        $this->routes[$route] = $params;
    }

    /**
     * @param string $url
     * @return bool
     */
    public function match(string $url): bool
    {
        foreach ($this->routes as $pattern => $route) {
            if ($url == $pattern) {
                $this->route = $route;
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $url
     */
    public function dispatch(string $url): void
    {
        if ($this->match($url)) {
            $controller = 'App\Controller\\' . $this->route['controller'];

            if (class_exists($controller)) {
                $cObj = new $controller();
                $action = $this->route['action'];

                if (method_exists($cObj, $action)) {
                    $cObj->$action(new Request);
                } else {
                    echo "Method <b>$action</b> not found";
                }
            } else {
                echo "Class <b>$controller</b> not found";
            }
        } else {
            http_response_code(404);
            require '404.html';
        }
    }
}