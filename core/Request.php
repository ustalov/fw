<?php
declare(strict_types=1);

namespace Core;

/*
 * Request
 */
class Request
{
    /**
     * Check request method type
     * @param string $method
     * @return bool
     */
    public function isMethod(string $method): bool
    {
        return strtolower($_SERVER['REQUEST_METHOD']) === $method ? true : false;
    }

    /**
     * Get GET variable
     * @param string $var
     * @return mixed
     */
    public function get(string $var)
    {
        return $_GET[$var] ?? null;
    }

    /**
     * Get POST variable
     * @param string $var
     * @return mixed
     */
    public function post(string $var)
    {
        return $_POST[$var] ?? null;
    }

    /**
     * Redirect to route
     * @param string $url
     * @param int $code
     */
    public function redirect(string $url, int $code = 301): void
    {
        header('Location: ' . $url, true, $code);
        exit();
    }

    /**
     * Check if ajax request
     * @return bool
     */
    public function isAjax(): bool
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }

    /**
     * Send ajax response
     * @param array $data
     * @param int $status
     * @return string
     */
    public function sendJson(array $data = [], int $status = 200): string
    {
        header('Content-Type: application/json');
        return json_encode($data);
    }
}