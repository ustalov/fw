<?php
define('ROOT', dirname(__DIR__));
define('APP', dirname(__DIR__) . '/app');

require ROOT . '/core/functions.php';
require ROOT . '/vendor/autoload.php';

use ScssPhp\ScssPhp\Compiler;

if (getenv('APPLICATION_ENV') == 'development') {
    $scss = new Compiler();
    $scss->setImportPaths('scss');
    $css = $scss->compile('@import "style.scss";');
    file_put_contents('css/style.css', $css);
}

$router = new Core\Router();

$router->add('product/list', ['controller' => 'Product', 'action' => 'list']);
$router->add('product/new', ['controller' => 'Product', 'action' => 'add']);
$router->add('product/delete', ['controller' => 'Product', 'action' => 'delete']);

$router->dispatch($_SERVER['QUERY_STRING']);