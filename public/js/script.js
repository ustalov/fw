$(function () {
    $("#product-add #type").change(function () {
        $("#special-attribute label").attr("class", "d-none");
        $("#special-attribute p").attr("class", "d-none");
        $("#special-attribute label#" + this.value + "-label").attr("class", "");
        $("#special-attribute p#" + this.value + "-note").attr("class", "");
        $("#special-attribute").attr("class", this.value ? "" : "d-none");
    });

    $("#product-add").one('submit', function (e) {
        e.preventDefault();

        $("#size-form.d-none").remove();
        $("#weight-form.d-none").remove();
        $("#dimension-form.d-none").remove();

        $(this).submit();
    });

    $(".dropdown-menu button").click(function (e) {
        e.preventDefault();

        $(".btn:first-child").text($(this).text());
        $(".btn:first-child").val($(this).text());
    });

    $("#product-list").submit(function (e) {
        if (!$(this).data("ajax")) return;

        e.preventDefault();

        var action = $("#action", this).attr("value");

        if (action === undefined) {
            return alert("Please select action.");
        }

        var ids = [];

        $(".products input[type=checkbox]", this).each(function () {
            if (this.checked) {
                ids.push($(this).val());
            }
        });

        if (!ids.length) {
            return alert("Please select product(s).");
        }

        $.ajax({
            type: "POST",
            url: "/product/delete",
            data: {ids: ids},
            success: function (res) {
                if (res.success) {
                    $.each(ids, function (i, id) {
                        $("div#product-" + id).remove();
                    });
                }
            }
        });
    });
});