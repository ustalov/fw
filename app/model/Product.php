<?php
declare(strict_types=1);

namespace App\Model;

/*
 * Product Model
 */
class Product extends \Core\Model
{
    /**
     * Table name -> products
     * @var string
     */
    public $table = 'products';

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $schema = [
            'id' => \PDO::PARAM_INT,
            'sku' => \PDO::PARAM_STR,
            'name' => \PDO::PARAM_STR,
            'price' => \PDO::PARAM_STR,
            'type' => \PDO::PARAM_STR,
            'special_attribute' => \PDO::PARAM_STR
        ];
        parent::__construct($schema);
    }

    /**
     * Product entity validate
     */
    public function validate(): void
    {
        $f = $this->fields;
        $sku = $this->findOne('sku', $f['sku']['value']);

        if ($sku) {
            $this->addError('sku', 'SKU must be unique');
        }

        if (empty($f['sku']['value'])) {
            $this->addError('sku', 'SKU is required');
        } elseif (!is_numeric($f['sku']['value'])) {
            $this->addError('sku', 'SKU must ne a number');
        }

        if (empty($f['name']['value'])) {
            $this->addError('name', 'Name is required');
        }

        if (empty($f['price']['value'])) {
            $this->addError('price', 'Price is required');
        } elseif (!is_numeric($f['price']['value'])) {
            $this->addError('price', 'Price must ne a number');
        }

        if (empty($f['type']['value'])) {
            $this->addError('type', 'Select a product type');
        }

        if ($f['type']['value'] == 'dvd-disc' && !is_numeric($f['special_attribute']['value'])) {
            $this->addError('special_attribute', 'Size is required and must be a number');
        }

        if ($f['type']['value'] == 'book' && !is_numeric($f['special_attribute']['value'])) {
            $this->addError('special_attribute', 'Weight is required and must be a number');
        }

        if ($f['type']['value'] == 'furniture' &&
            !preg_match('/^(\d+)x(\d+)x(\d+)/', $f['special_attribute']['value'])) {
            $this->addError('special_attribute', 'Dimensions format is wrong');
        }
    }
}