<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Product controller
 */
class Product extends \Core\Controller
{

    /**
     * List all products
     */
    public function list(): void
    {
        $model = new \App\model\Product;
        $data = $model->findAll();
        $title = 'Product List';
        $this->renderTemplate('Product/list', compact('title', 'data'));
    }

    /**
     * Add new product
     * @param \Core\Request $req
     */
    public function add(\Core\Request $req): void
    {
        $errors = [];

        if ($req->isMethod('post')) {
            $model = new \App\model\Product;
            $model->sku = $req->post('sku');
            $model->name = $req->post('name');
            $model->price = $req->post('price');
            $model->type = $req->post('type');
            $model->special_attribute = $req->post('special_attribute');
            $model->validate();
            $errors = $model->getErrors();

            if (!$errors) {
                $model->save();
                $req->redirect('/product/list');
            }
        }

        $title = 'Product Add';
        $this->renderTemplate('Product/add', compact('title', 'errors'));
    }

    /**
     * Delete one or mass product(s)
     * @param \Core\Request $req
     */
    public function delete(\Core\Request $req): void
    {
        if ($req->isMethod('post')) {
            $model = new \App\model\Product;
            $model->delete($_POST['ids']);
        }

        if ($req->isAjax()) {
            echo $req->sendJson(['success' => true]);
        }
    }
}