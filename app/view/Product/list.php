<section>
    <form method="POST" id="product-list" data-ajax="true">
        <header class="blog-header pt-3">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4 pt-1">
                    <h1><?=$title?></h1>
                </div>
                <div class="col-8 d-flex justify-content-end align-items-center">
                    <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle" type="button" id="action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <button class="dropdown-item" data-value="delete">Delete</button>
                        </div>
                    </div>
                    <button type="submit" id="apply" class="btn btn-light ml-1">Apply</button>
                </div>
            </div>
        </header>
        <hr>
        <?php if (!empty($data)): ?>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 products">
                <?php foreach ($data as $p): ?>
                    <div id="product-<?=$p['id']?>" class="col product">
                        <div class="border m-1 p-2">
                            <div class="text-left">
                                <input type="checkbox" id="" name="product[]" value="<?=$p['id']?>">
                            </div>
                            <div class="text-center">
                                <p><?=$p['sku']?></p>
                                <p><?=$p['name']?></p>
                                <p><?=$p['price']?> $</p>
                                <p><?=$p['special_attribute'] . ($p['type'] == 'dvd-disc' ? ' MB' : ($p['type'] == 'book' ? ' KG' : '')) ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </form>
</section>