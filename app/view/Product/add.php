<section>
    <form method="POST" id="product-add" data-ajax="false">
        <header class="blog-header pt-3">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4 pt-1">
                    <h1><?=$title?></h1>
                </div>
                <div class="col-8 d-flex justify-content-end align-items-center">
                    <button type="submit" id="save" class="btn btn-light ml-1">Save</button>
                </div>
            </div>
        </header>
        <hr>
        <div class="col-sm-8 col-md-6 col-lg-4">
            <?php if (!empty($errors)): ?>
                <div class="alert alert-danger" role="alert">
                    <?php foreach ($errors as $error): ?>
                        <p><?=$error?></p>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <label for="sku">SKU</label>
                <input type="text" name="sku" id="sku" class="form-control">
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                <input type="text" name="price" id="price" class="form-control">
            </div>
            <div class="form-group">
                <label for="type">Type</label>
                <select name="type" id="type" class="form-control">
                    <option value="">Select type ...</option>
                    <option value="dvd-disc">DVD-disc</option>
                    <option value="book">Book</option>
                    <option value="furniture">​​Furniture</option>
                </select>
            </div>
            <div id="special-attribute" class="d-none">
                <div class="form-group">
                    <label for="attribute" id="dvd-disc-label" class="d-none">Size</label>
                    <label for="attribute" id="book-label" class="d-none">Weight</label>
                    <label for="attribute" id="furniture-label" class="d-none">Dimensions</label>
                    <input type="text" name="special_attribute" id="attribute" class="form-control">
                </div>
                <p id="dvd-disc-note" class="d-none">Please enter size</p>
                <p id="book-note" class="d-none">Please enter weight</p>
                <p id="furniture-note" class="d-none">Please provide dimensions in HxWxL format</p>
            </div>
        </div>
    </form>
</section>